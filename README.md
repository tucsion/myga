# 开发环境
### gulp version
```
CLI version: 2.2.0
Local version: 4.0.2
```

### node version
```
v12.12.0
```

# 目录
```
├── dev                     // 开发运行时文件，可以理解为runtime文件
│ ├── css                   // css文件，less2css | autoprefixer
│ ├── images                // 图片文件
│ ├── js                    // js文件， babel（es6转es5）
│ ├── vendor                // 插件目录
│ ├── demo1.html            // 测试html
│ ├── index.html            //测试首页

├── dist                    // 最终的build目录，这个是您要的^_^
│ ├── css                   // css 文件，进过useref合并压缩
│ ├── images                
│ ├── js                    // js 经过uglify合并压缩
│ ├── vendor
│ ├── demo1.html           
│ ├── index.html            // head里面的css已合并，IE兼容js已合并

├── src                     // 开发目录，你发开的文件都在这里
│ ├── css                   // css，已经存在的css直接放这里
│ ├── images
│ ├── inc                   // 公用文件目录，如：header.html,footer.html
│ ├── js
│ ├── less                  // 需要开发的css，通过编写less实现，也可以通过sass实现（要修改插件）
│ ├── vendor                // 插件
│ ├── demo1.html            // 测试html
│ ├── index.html            // 测试首页html
│ ├── tasks                 // 所有的gulp任务都在这里，被拆成了一个个单独文件
│ ├── gulpfile.js
│ ├── .gitignore
│ ├── package.json
│ ├── README.md             // 就是我啦，你正在看
```

# 启动与打包

- 启动
> npm install
> gulp

- 打包
> gulp build

# 实现了几个功能

- 拆分gulpfile.js，将不同类型的task拆分成单独的js，放到./tasks里面，方便管理和修改，不然记在一个文件里面真的很爽

- 实现了less2css

- 实现了css合并、压缩

- 实现了本地服务，用的的 `gulp-connect`，开发环境是 ==http://localhost:8080== ，build环境是 ==http://localhos:8081== ;
> `server`搭配`watch`实现修改文件后页面自动刷新，前端攻城狮的最爱，如果你身边还有另一个显示器，可以实现一个屏幕code，一个屏幕view的效果
> `server`可以通过配置`host`，让同一个局域网的手机访问电脑上的网站
> 以上，就可以一个电脑code，一个显示器显示PC效果，一个手机显示mobile效果，且是同步的，自动刷新的，您只管努力code就行了

- 实现了类似后端的文件引入（include），src/index.html 和 src/demo1.html 都通过@@include标签引入了 `header.html`, 这样没有服务支持，也可以随便点了
> 同理，可以实现`footer.html`等任意公用文件的引入，公用文件在 src/inc中

- 通过`gulp-useref`实现了html页面共用css和js的合并。

- js文件没有做过多操作，仅实现了多个js合并、压缩
