/**
 * @ Author: zhengbo
 * @ Date: 2020-05-15 02:25:52
 * @ LastEditTime: 2020-05-16 00:24:36
 * @ LastEditors: zhengbo
 * @ Description: 本地服务
 * @ FilePath: /gulp/tasks/server.js
 * @ Contact:78501051@qq.com
 */

/**
 * @ description: dev环境
 * @ 可以通过修改host参数，实现手机访问的功能，pc和手机需在一个wifi下
 * @ param {type} 
 * @ return: 
 */
gulp.task('server:dev', function () {
    connect.server({
        //host: '192.168.1.110', //地址，可不写，不写的话，默认localhost
        port: 8080, //端口号，可不写，默认8000
        root: DEV, //当前项目主目录
        livereload: true, //自动刷新
        host: '::',
    })
})

/**
 * @ description: build后服务
 * @ 用于检查打包后效果
 * @ param {type} 
 * @ return: 
 */
gulp.task('server:build', function () {
    connect.server({
        //host: '192.168.1.110', //地址，可不写，不写的话，默认localhost
        port: 8081, //端口号，可不写，默认8000
        root: DIST, //当前项目主目录
        livereload: true, //自动刷新
        host: '::',
    })
})