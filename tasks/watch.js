/**
 * @ Author: zhengbo
 * @ Date: 2020-05-15 03:34:46
 * @ LastEditTime: 2020-05-16 00:18:45
 * @ LastEditors: zhengbo
 * @ Description: 监听文件变化
 * @ FilePath: /gulp/tasks/watch.js
 * @ Contact:78501051@qq.com
 */

gulp.task('watch', function () {
    gulp.watch([
        SRC + '/less/*.less',
    ], gulp.series('css:dev:less2css'))

    gulp.watch([
        SRC + '/css/*.css',
    ], gulp.series('css:dev:move'))

    gulp.watch([
        SRC + '/js/*.js',
    ], gulp.series('script:dev:move'))

    gulp.watch([
        SRC + '/images/**/*',
    ], gulp.series('images:dev:move'))

    gulp.watch([
        SRC + '/**/*.html',
        !SRC + '/vendor/*'
    ], gulp.series('html:dev:move'))

    gulp.watch([
        SRC + '/vendor/**/*'
    ], gulp.series('vendor:dev:move'))
})