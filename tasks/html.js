/**
 * @ Author: zhengbo
 * @ Date: 2020-05-15 03:40:23
 * @ LastEditTime: 2020-05-16 00:23:43
 * @ LastEditors: zhengbo
 * @ Description: html处理文件
 * @ FilePath: /gulp/tasks/html.js
 * @ Contact:78501051@qq.com
 */
const fileinclud = require('gulp-file-include')
const useref = require('gulp-useref')
const uglify = require('gulp-uglify')
const cleanCss = require('gulp-clean-css')
const gulpIf = require('gulp-if')

/**
 * @ description: 开发环境下处理html文件
 * @ param {type} 
 * @ return: 
 */
gulp.task('html:dev:move', () => {
    return gulp.src([
        SRC + '/**/*.html',
        '!' + SRC + '/vendor/**',
        '!' + SRC + '/inc/**'
    ])
        // 通过gulp-file-include插件，进行文件引入
        .pipe(fileinclud({
            prefix: '@@',//变量前缀 @@include
            basepath: SRC + '/inc',//引用文件路径
            indent: true//保留文件的缩进
        }))
        .pipe(gulp.dest(DEV))
        .pipe(connect.reload())
})

/**
 * @ description: 移动html文件，从dev目录到dist目录
 * @ param {type} 
 * @ return: 
 */
gulp.task('html:build:move', () => {
    return gulp.src([
        DEV + '/**/*.html',
        '!' + DEV + '/vendor/**'
    ])
        // 通过gulp-useref插件，合并css，js文件
        .pipe(useref())

        // 判断如果是js，进行混淆压缩
        .pipe(gulpIf('*.js',
            uglify({
                mangle: true,//类型：Boolean 默认：true 是否修改变量名
                compress: true,//类型：Boolean 默认：true 是否完全压缩
                //preserveComments: all //保留所有注释
            })

        ))

        // 判断是css，进行压缩
        .pipe(gulpIf('*.css', cleanCss()))

        .pipe(gulp.dest(DIST))
        .pipe(connect.reload())
})
