/**
 * @ Author: zhengbo
 * @ Date: 2020-05-15 02:51:50
 * @ LastEditTime: 2020-05-16 00:20:19
 * @ LastEditors: zhengbo
 * @ Description: 删除文件
 * @ FilePath: /gulp/tasks/clean.js
 * @ Contact:78501051@qq.com
 */

const del = require('del')

gulp.task('clean:dev', () => {
    return del(DEV)
})

gulp.task('clean:dist', () => {
    return del(DIST)
})