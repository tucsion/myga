/**
 * @ Author: zhengbo
 * @ Date: 2020-05-15 15:27:43
 * @ LastEditTime: 2020-05-16 00:18:12
 * @ LastEditors: zhengbo
 * @ Description: 移动vendor文件夹
 * @ FilePath: /gulp/tasks/vendor.js
 * @ Contact:78501051@qq.com
 */

gulp.task('vendor:dev:move', () => {
    return gulp.src(SRC + '/vendor/**/*')
        .pipe(gulp.dest(DEV + '/vendor'))
        .pipe(connect.reload())
})

gulp.task('vendor:build:move', () => {
    return gulp.src(DEV + '/vendor/**/*')
        .pipe(gulp.dest(DIST + '/vendor'))
        .pipe(connect.reload())
})