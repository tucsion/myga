/**
 * @ Author: zhengbo
 * @ Date: 2020-05-15 02:55:11
 * @ LastEditTime: 2020-05-16 00:20:51
 * @ LastEditors: zhengbo
 * @ Description: 编译less，移动css
 * @ FilePath: /gulp/tasks/css.js
 * @ Contact:78501051@qq.com
 */
const gulpLess = require('gulp-less')
const autoprefixer = require('gulp-autoprefixer')


gulp.task('css:dev:less2css', () => {
    return gulp.src([
        SRC + '/less/*.less'
    ])
        .pipe(gulpLess())
        .pipe(autoprefixer())
        .pipe(gulp.dest(DEV + '/css'))
        .pipe(connect.reload())
})


gulp.task('css:dev:move', () => {
    return gulp.src([
        SRC + '/css/*.css'
    ])
        .pipe(autoprefixer())
        .pipe(gulp.dest(DEV + '/css'))
        .pipe(connect.reload())
})