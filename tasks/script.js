/**
 * @ Author: zhengbo
 * @ Date: 2020-05-15 15:38:06
 * @ LastEditTime: 2020-05-16 00:24:08
 * @ LastEditors: zhengbo
 * @ Description: js文件处理
 * @ FilePath: /gulp/tasks/script.js
 * @ Contact:78501051@qq.com
 */
const babel = require('gulp-babel')

gulp.task('script:dev:move', () => {
    return gulp.src(SRC + '/js/**/*')
        .pipe(babel({
            "presets": "env"
        }))
        .pipe(gulp.dest(DEV + '/js'))
        .pipe(connect.reload())
})