/**
 * @ Author: zhengbo
 * @ Date: 2020-05-15 15:36:08
 * @ LastEditTime: 2020-05-16 00:23:58
 * @ LastEditors: zhengbo
 * @ Description: 图片处理文件
 * @ FilePath: /gulp/tasks/images.js
 * @ Contact:78501051@qq.com
 */


gulp.task('images:dev:move', () => {
    return gulp.src(SRC + '/images/**/*')
        .pipe(gulp.dest(DEV + '/images'))
        .pipe(connect.reload())
})


gulp.task('images:build:move', () => {
    return gulp.src(DEV + '/images/**/*')
        .pipe(gulp.dest(DIST + '/images'))
        .pipe(connect.reload())
})