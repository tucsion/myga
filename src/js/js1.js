/*
 * Define our tasks using plain functions
 */
function styles() {
    return gulp.src(paths.styles.src)
        .pipe(less())
        .pipe(cleanCSS())
        // pass in options to the stream
        .pipe(rename({
            basename: 'main',
            suffix: '.min'
        }))
        .pipe(gulp.dest(paths.styles.dest));
}

var a = 1
var b = 2

var arr = [1, 3, 4]
for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    console.log(element);
}

const fu = () => {
    console.log('es6 fn');

}

(() => {
    console.log('es6测试');
})()

const aa = 123