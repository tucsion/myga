const requireDir = require('require-dir')
global.gulp = require('gulp')
global.path = require('path')

/**
 * 环境变量
 */
global.ENV = 'dev' // dev|build


/**
 * 文件路径
 */
global.SRC = './src'
global.DEV = './dev'
global.DIST = './dist'


/**
 * 本地服务
 */
global.connect = require('gulp-connect')

requireDir('tasks', { recurse: true })

/**
 * 开发环境
 */
const dev = gulp.series(
    'clean:dev',
    gulp.parallel(
        'css:dev:less2css',
        'css:dev:move',
        'script:dev:move',
        'images:dev:move',
        'vendor:dev:move',
        'html:dev:move'
    ),
    gulp.parallel(
        'server:dev',
        'watch'
    )
)

/**
 * build打包
 */
const build = gulp.series(
    'clean:dist',
    'images:build:move',
    'vendor:build:move',
    'html:build:move',
    'server:build'
)

exports.build = build
exports.default = dev